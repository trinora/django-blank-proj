Contributors
============

The following contributors have all licensed their contributions to this project 
under the licensing terms detailed in the LICENSE.md file.

* MyName LastName <my.email@emailaddress.com> FIXME

_If you are newly contributing to this project, please add your contact 
information to the list above._

Acknowledgements
================

The contributors above would like to acknowledge the efforts of:

* Arthur Lucas <alucas1@trinora.com>: blank Django project template