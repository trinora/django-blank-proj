#  _____     _                       
# |_   _| __(_)_ __   ___  _ __ __ _ 
#   | || '__| | '_ \ / _ \| '__/ _` |
#   | || |  | | | | | (_) | | | (_| |
#   |_||_|  |_|_| |_|\___/|_|  \__,_|
#                                    
# This program is licensed under the terms detailed in the LICENSE.md file.

from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'blank_project.views.home', name='home'),
	# url(r'^blank_project/', include('blank_project.foo.urls')),
	
	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	
	# Uncomment the next line to enable the admin:
	# url(r'^admin/', include(admin.site.urls)),
)
