#  _____     _                       
# |_   _| __(_)_ __   ___  _ __ __ _ 
#   | || '__| | '_ \ / _ \| '__/ _` |
#   | || |  | | | | | (_) | | | (_| |
#   |_||_|  |_|_| |_|\___/|_|  \__,_|
#                                    
# This program is licensed under the terms detailed in the LICENSE.md file.

"""
Loads configuration from a collection of *.conf files within this directory. 
This is useful for splitting out setting values into separate files and keeping
(for instance) production and test values independent and easier to manage.
"""

__author__ = "Arthur Lucas"		# FIXME
__email__ = "alucas1@trinora.com"		# FIXME
__copyright__ = "Copyright 2013 Trinora, Inc. All rights reserved."

import os.path
import glob

conf_files = glob.glob(os.path.join(os.path.dirname(__file__), "settings", "*.conf"))
conf_files.sort()

for f in conf_files:
    execfile(os.path.abspath(f))