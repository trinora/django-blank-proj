#!/usr/bin/env python

#  _____     _                       
# |_   _| __(_)_ __   ___  _ __ __ _ 
#   | || '__| | '_ \ / _ \| '__/ _` |
#   | || |  | | | | | (_) | | | (_| |
#   |_||_|  |_|_| |_|\___/|_|  \__,_|
#                                    
# This program is licensed under the terms detailed in the LICENSE.md file.

"""
Django management.
"""

__author__ = "Arthur Lucas"		# FIXME
__email__ = "alucas1@trinora.com"		# FIXME
__copyright__ = "Copyright 2013 Trinora, Inc. All rights reserved."

import os
import sys

if __name__ == "__main__":
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blank_project.settings")
	
	from django.core.management import execute_from_command_line
	
	execute_from_command_line(sys.argv)
